package com.example.xhader.adapters;

import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.xhader.R;
import com.example.xhader.entities.FileItem;
import com.example.xhader.utils.OnFileItemAction;

import java.util.List;

// This Class contain Adapter of FileList. This class is used to add information to fileItem container which is file_item.xml
public class FileItemAdapter extends BaseAdapter {
    private Context context;
    private List<FileItem> fileItemList;
    private LayoutInflater inflater;
    private  String currentUserName;

    public FileItemAdapter(Context context, List<FileItem> fileItemList,String currentUserName) {
        this.context = context;
        this.fileItemList = fileItemList;
        this.inflater = LayoutInflater.from(context);
        this.currentUserName = currentUserName;


    }

    @Override
    public int getCount() {
        return fileItemList.size();
    }

    @Override
    public Object getItem(int position) {
        return fileItemList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = inflater.inflate(R.layout.file_item,null);

        //Get Current File Informations
        FileItem currentFile = (FileItem) getItem(position);
        String fileId = currentFile.getFileId();
        String fileName =currentFile.getFileName();
        String fileSize = currentFile.getFileSize();
        String fileSender = currentFile.getFileSender();
        String fileType = currentFile.getFileType();
        String fileDate = currentFile.getFileDate();
        String fileUrl = currentFile.getFileUrl();


        // Get Container Information

        //Name of current File
        String checker_name = new OnFileItemAction().getFileName(fileName);
        TextView fileNameContainer = convertView.findViewById(R.id.file_name);
        fileNameContainer.setText(checker_name);

        // Type of the File

        String checker_type = new OnFileItemAction().getFileExtension(fileName);
        TextView fileTypeContainer = convertView.findViewById(R.id.file_type);
        fileTypeContainer.setText(checker_type);

        /// Image of Current File
        ImageView fileImage = convertView.findViewById(R.id.file_image);
        String checker_image =new OnFileItemAction().getItemType(fileType,fileName);
        if(checker_image.equalsIgnoreCase("image")){
            Glide.with(context).load(Uri.parse(fileUrl)).into(fileImage);
        }
        else {
            String resourcedName ="ic_"+checker_image+"_item";
            int res =context.getResources().getIdentifier(resourcedName,"drawable",context.getPackageName());
            fileImage.setImageResource(res);
        }

        // File SenderName
        String checker_sender = new  OnFileItemAction().getSenderName(fileSender,currentUserName);
        TextView fileSenderContainer = convertView.findViewById(R.id.file_sender);
        if (checker_sender.equalsIgnoreCase("Moi")){

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                fileSenderContainer.setTextAppearance(R.style.TextAppearance_AppCompat_Medium);
                fileSenderContainer.setTextColor(Color.rgb(0,153,255));
            }
            fileSenderContainer.setText(checker_sender);
        }
        else{
            fileSenderContainer.setText(checker_sender);
        }

        return convertView;
    }
}
