package com.example.xhader;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.example.xhader.action.UserAction;
import com.example.xhader.callback.FragmentListener;
import com.example.xhader.entities.User;
import com.example.xhader.fragments.ForgetPasswordFragment;
import com.example.xhader.fragments.LoginFragment;
import com.example.xhader.fragments.ProfileFragment;
import com.example.xhader.fragments.RegisterFragment;

public class PreviousMainActivity extends AppCompatActivity implements FragmentListener {
    private UserAction userAction;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_previous_main);
        userAction = new UserAction();
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container_fragment, LoginFragment.newInstance())
                .commitNow();
    }

    @Override
    public void changeFragment(String page) {
        if(page == "register"){
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container_fragment, RegisterFragment.newInstance())
                    .commitNow();
        }

        else if(page == "login"){
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container_fragment, LoginFragment.newInstance())
                    .commitNow();
        }

        else{
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container_fragment, ForgetPasswordFragment.newInstance())
                    .commitNow();
        }
    }

    @Override
    public void sendRegisterInformation(final String pseudo, final String email, final String password, final String confirm_pass) {

        new Thread(new Runnable() {
            @Override
            public void run() {
                final String message =userAction.registerUser(PreviousMainActivity.this,pseudo,email,password,confirm_pass);

                Handler handler=new Handler(Looper.getMainLooper());
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        if (message== "win"){
                            Log.d("Start Login  ", "Démarrage de la page de connexion");
                            changeFragment("login");

                        }
                    }
                });
            }
        }).start();

    }

    @Override
    public void sendLoginInformation(final String pseudo, final String password) {

        new  Thread(new Runnable() {
            @Override
            public void run() {

                final User stateUser= userAction.loginUser(PreviousMainActivity.this,pseudo,password);
                Log.d("Try connection of user ", "Mon utilisateur" +stateUser.getPseudo() + stateUser.getId_user());
                Handler handler=new Handler(Looper.getMainLooper());
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        if (stateUser.getPseudo()!=null){
                            Log.d("Start MainActivity  ", "Démarrage de la main activity");
                            Intent intent = new Intent(PreviousMainActivity.this,MainActivity.class);
                            intent.putExtra("id",stateUser.getId_user());
                            intent.putExtra("email",stateUser.getEmail());
                            intent.putExtra("pseudo",stateUser.getPseudo());
                            intent.putExtra("password",stateUser.getPassword());
                            startActivity(intent);
                            finish();

                    }
                }
            });
        }
        }).start();

    }

    @Override
    public void sendUpdatePasswordInformation(final String pseudo, final String password) {

        new Thread(new Runnable() {
            @Override
            public void run() {

                final String message= userAction.changerUserPassword(PreviousMainActivity.this,pseudo,password);
                Log.d("TryChangePasswordOfUser", "Mon utilisateur" +pseudo+ " "+ password );
                Handler handler=new Handler(Looper.getMainLooper());
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        if (message== "win"){
                            Log.d("Start Login  ", "Démarrage de la page de connexion");
                            changeFragment("login");
                        }
                    }
                });


            }
        }).start();

    }


}