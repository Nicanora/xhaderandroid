package com.example.xhader.entities;


// This is FileItem class which contain all attributes of file which will be send to server
public class FileItem {
    private String fileId;
    private String fileName;
    private  String fileSize;
    private String fileSender;
    private  String fileType;
    private  String fileUrl;
    private  String fileDate;

    public FileItem(String fileId,String fileName,String fileType, String fileSize, String fileSender, String fileUrl, String fileDate) {
        this.fileId = fileId;
        this.fileName = fileName;
        this.fileType = fileType;
        this.fileSize = fileSize;
        this.fileSender = fileSender;
        this.fileUrl = fileUrl;
        this.fileDate = fileDate;

    }

    public String getFileName() {
        return fileName;
    }

    public String getFileId() {
        return fileId;
    }

    public String getFileType() {
        return fileType;
    }



    public String getFileSize() {
        return fileSize;
    }

    public String getFileDate() {
        return fileDate;
    }

    public String getFileSender() {
        return fileSender;
    }

    public String getFileUrl() {
        return fileUrl;
    }
}
