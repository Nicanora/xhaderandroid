package com.example.xhader.entities;

// User class is all user attributes and properties
public class User {
    /**
     * Default constructor
     */
    public User() {
    }

    /**
     * L'id de l'utilisateur
     */
    private  String id_user;

    /**
     * Le pseudo de l'utlisateur
     */
    private String pseudo;

    /**
     * Le mot de passse de l'utlisateur
     */
    private String password;

    /**
     * L' email de l'utlisateur
     */
    private String email;


    public String getPseudo(){return pseudo;}
    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }

    public String getPassword(){return password;}
    public void setPassword(String password) {
        this.password = password;
    }

    public   String getEmail(){return email;}
    public void setEmail(String email) {
        this.email = email;
    }

    public String getId_user() {
        return id_user;
    }

    public void setId_user(String id_user) {
        this.id_user = id_user;
    }

    @Override
    public String toString() {
        return "User{" +
                ", id=" + id_user +
                "pseudo='" + pseudo + '\'' +
                ", password=" + password +
                ", email=" + email +
                '}';
    }
}
