package com.example.xhader;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;

import com.example.xhader.callback.FragmentListener;
import com.example.xhader.callback.LogoutListener;
import com.example.xhader.fragments.ForgetPasswordFragment;
import com.example.xhader.fragments.HomeFileFragment;
import com.example.xhader.fragments.ProfileFragment;
import com.google.android.material.navigation.NavigationView;

public class MainActivity extends AppCompatActivity implements LogoutListener {

    DrawerLayout drawerLayout;
    ActionBarDrawerToggle toggle;
    Toolbar toolbar;
    NavigationView navigationView;
    private TextView user_name, user_email;
    private String[] user_info;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.frame,HomeFileFragment.newInstance())
                .commit();

        drawerLayout = findViewById(R.id.drawer);
        toolbar = findViewById(R.id.toolBar);
        setSupportActionBar(toolbar);
        toggle = new ActionBarDrawerToggle(this,drawerLayout,toolbar,R.string.open,R.string.close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();
        navigationView = findViewById(R.id.nav_view);

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {

            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int id = item.getItemId();
                Fragment fragment = null;
                switch (id)
                {
                    case  R.id.home:
                        fragment = new HomeFileFragment();
                        loadFragment(fragment);
                        break;
                    case  R.id.profil:
                        fragment = new ProfileFragment();
                        loadFragment(fragment);
                        break;

                    case  R.id.settings:
                        break;
                    case  R.id.help:
                        break;
                    default:
                        return true;
                }
                return true;
            }
        });
    }



    private void loadFragment(Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        fragmentTransaction.replace(R.id.frame,fragment)
                .addToBackStack(null)
                .commit();
        drawerLayout.closeDrawer(GravityCompat.START);
        fragmentTransaction.addToBackStack(null);

    }


    public String[] userInfo(){

        String [] all_user_info = {getIntent().getStringExtra("id"),
                getIntent().getStringExtra("pseudo"),
                getIntent().getStringExtra("email"),
                getIntent().getStringExtra("password")};
        return all_user_info;
    }


    @Override
    public void logout() {
        Intent intent = new Intent(MainActivity.this, PreviousMainActivity.class);
        startActivity(intent);
        finish();
    }
}