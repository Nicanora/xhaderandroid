package com.example.xhader.fragments;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.xhader.MainActivity;
import com.example.xhader.R;
import com.example.xhader.callback.FragmentListener;
import com.example.xhader.callback.LogoutListener;


public class ProfileFragment extends Fragment implements View.OnClickListener {

    TextView logout,profileName,profileEmail;
    LogoutListener logoutListener;
    String [] user_info;

    public ProfileFragment() {
        // Required empty public constructor
    }


    public static ProfileFragment newInstance() {
        return new ProfileFragment();
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_profile, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        user_info = getUserInfo();
        logout = view.findViewById(R.id.logout);
        profileEmail = view.findViewById(R.id.profil_email);
        profileName = view.findViewById(R.id.profil_name);

        profileName.setText(user_info[1]);
        profileEmail.setText(user_info[2]);

        logout.setOnClickListener(this);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof LogoutListener){
            Log.d("Action of Attach","Attach Action Success");
            logoutListener = (LogoutListener) context;

        }
        else {
            Log.d("Action of Attach","Attach Action Failed");
        }
    }

    @Override
    public void onClick(View v) {
        if (logoutListener!=null){

            logoutListener.logout();

        }
        else {
            Log.d("Action of Click","Click Action Success but attach failed");
        }

    }


    public String [] getUserInfo() {
        MainActivity mainActivity=(MainActivity)getActivity();
        String user_Info[]=mainActivity.userInfo();
        return user_Info;
    }
}