package com.example.xhader.fragments;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.xhader.R;
import com.example.xhader.callback.FragmentListener;


public class ForgetPasswordFragment extends Fragment implements View.OnClickListener {

    private TextView create_accountbyforget;
    Button btn_forget;
    EditText pseudo_forget, password_forget;
    private FragmentListener mListener;


    public ForgetPasswordFragment() {
        // Required empty public constructor
    }

    public static ForgetPasswordFragment newInstance() {
        return new ForgetPasswordFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_forget_pasword, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        create_accountbyforget = view.findViewById(R.id.loginby_forget);

        pseudo_forget = view.findViewById(R.id.pseudo_forget);
        password_forget = view.findViewById(R.id.password_forget);
        btn_forget = view.findViewById(R.id.button_forget_password);

        btn_forget.setOnClickListener(this);
        create_accountbyforget.setOnClickListener(this);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof FragmentListener){
            Log.d("Action of Attach","Attach Action Success");
            mListener = (FragmentListener) context;
        }
        else {
            Log.d("Action of Attach","Attach Action Failed");
        }
    }

    @Override
    public void onClick(View v) {
        String pseudo = pseudo_forget.getText().toString().trim();
        String password = password_forget.getText().toString().trim();

        if (mListener!=null){
            if (v ==btn_forget){
                mListener.sendUpdatePasswordInformation(pseudo,password);
                Log.d("Action of Click","Button forget Password");
            }

            else if (v == create_accountbyforget){
                mListener.changeFragment("register");
                Log.d("Action of Click","Button Create Account");
            }

        }
        else {
            Log.d("Action of Click","Click Action Success but attach failed");
        }
    }
}