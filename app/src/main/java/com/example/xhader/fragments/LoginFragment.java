package com.example.xhader.fragments;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.xhader.R;
import com.example.xhader.callback.FragmentListener;


public class LoginFragment extends Fragment implements View.OnClickListener {


    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private TextView forgot_password,create_accountbylogin;
    Button btn_login;
    EditText pseudo_login, password_login;
    private FragmentListener mListener;



    public LoginFragment() {
        // Required empty public constructor
    }

    public static LoginFragment newInstance() {

        return new LoginFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_login, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        forgot_password = view.findViewById(R.id.forgot_password);
        create_accountbylogin = view.findViewById(R.id.create_accountbylogin);
        pseudo_login = view.findViewById(R.id.pseudo_login);
        password_login = view.findViewById(R.id.password_login);
        btn_login = view.findViewById(R.id.button_login);

        btn_login.setOnClickListener(this);
        create_accountbylogin.setOnClickListener(this);
        forgot_password.setOnClickListener(this);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof FragmentListener){
            Log.d("Action of Attach","Attach Action Success");
            mListener = (FragmentListener) context;
        }
        else {
            Log.d("Action of Attach","Attach Action Failed");
        }
    }

    @Override
    public void onClick(View v) {
        String pseudo = pseudo_login.getText().toString().trim();
        String password = password_login.getText().toString().trim();
        if (mListener!=null){
            if (v ==forgot_password){
                mListener.changeFragment("forgot");
                Log.d("Action of Click","Button forget Password");
            }

            else if (v == create_accountbylogin){
                mListener.changeFragment("register");
                Log.d("Action of Click","Button Create Account");
            }

            else if (v == btn_login){
                mListener.sendLoginInformation(pseudo,password);
                Log.d("Action of Click","Button Login");
            }

        }
        else {
            Log.d("Action of Click","Click Action Success but attach failed");
        }

    }

}