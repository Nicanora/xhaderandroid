package com.example.xhader.fragments;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.xhader.R;
import com.example.xhader.callback.FragmentListener;


public class RegisterFragment extends Fragment  implements View.OnClickListener {

    private TextView loginRegisterPage;
    private EditText pseudo_register,email_register,password_register,confirm_pass;
    Button sign_btn;
    private FragmentListener mListener;



    public RegisterFragment() {
        // Required empty public constructor
    }


    public static RegisterFragment newInstance() {
        return  new RegisterFragment();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_register, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loginRegisterPage = view.findViewById(R.id.loginbycreateaccount);
        pseudo_register = view.findViewById(R.id.pseudo_sign);
        email_register = view.findViewById(R.id.email_sign);
        password_register = view.findViewById(R.id.password_sign);
        confirm_pass = view.findViewById(R.id.confirm_password);
        sign_btn = view.findViewById(R.id.button_sign);
        loginRegisterPage.setOnClickListener(this);
        sign_btn.setOnClickListener(this);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof FragmentListener){
            Log.d("Action of Attach","Attach Action Success");
            mListener = (FragmentListener) context;
        }
        else {
            Log.d("Action of Attach","Attach Action Failed");
        }
    }

    @Override
    public void onClick(View v) {
        if (mListener!=null){
            if (v == loginRegisterPage){
                mListener.changeFragment("login");
                Log.d("Action of Click","Button Login");
            }
            else if (v == sign_btn){
                String pseudo = pseudo_register.getText().toString().trim();
                String email = email_register.getText().toString().trim();
                String pass = password_register.getText().toString().trim();
                String pass_confirm = confirm_pass.getText().toString().trim();
                mListener.sendRegisterInformation(pseudo,email,pass,pass_confirm);
            }

        }
        else {
            Log.d("Action of Click","Click Action Success but attach failed");
        }
    }
}