package com.example.xhader.fragments;

import android.Manifest;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import android.os.Handler;
import android.os.Looper;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.xhader.MainActivity;
import com.example.xhader.R;
import com.example.xhader.action.UserAction;
import com.example.xhader.adapters.FileItemAdapter;
import com.example.xhader.entities.FileItem;
import com.example.xhader.utils.FilePathSender;
import com.example.xhader.utils.GetListViewFile;
import com.example.xhader.utils.OnFileItemAction;
import com.example.xhader.utils.UploadFileToServer;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static android.app.Activity.RESULT_OK;

public class HomeFileFragment extends Fragment implements View.OnClickListener {

    // the fragment initialization parameters

    private FloatingActionButton upload_button;
    private UploadFileToServer uploadFileToServer;
    private static final int MY_REQUEST_CODE_PERMISSION = 1;
    private static final int MY_RESULT_CODE_FILECHOOSER = 2;
    private static final String LOG_TAG = "Choose File";
    private List<FileItem> fileItemList;
    private GetListViewFile getListViewFile;



    public HomeFileFragment() {
        // Required empty public constructor
    }

    public static HomeFileFragment newInstance() {
        return new HomeFileFragment();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home_file, container, false);
    }

    @Override
    public void onViewCreated(@NonNull final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.upload_button = view.findViewById(R.id.upload_file_button);
        this.fileItemList = new ArrayList<FileItem>();
        final Context context = this.getContext();
        getListViewFile = new GetListViewFile();
        try {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    final List<FileItem> fileItemList = getListViewFile.GetFile(context);
                    Log.d("Notre liste","Liste de taille "+ fileItemList.size());


                    Handler handler = new Handler(Looper.getMainLooper());
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            if (fileItemList.size()<1){
                                showToast("Aucun fichier n'a encore été enrégistré dans la base de données. Veuillez en ajouter un.");
                            }
                            else{
                                GridView file_list_view = (GridView) view.findViewById(R.id.file_list_view);
                                file_list_view.setAdapter(new FileItemAdapter(context,fileItemList,getUserInfo()[1]));
                                file_list_view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                    @Override
                                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                        FileItem currentFile = fileItemList.get(position);
                                        String checker_view = new OnFileItemAction().getItemType(currentFile.getFileType(),currentFile.getFileName());
                                        StartIntentToViewFile(checker_view,currentFile.getFileUrl());

                                    }
                                });

                                file_list_view.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                                    @Override
                                    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                                        final FileItem currentFile = fileItemList.get(position);
                                        final Dialog dialog = new Dialog(context);
                                        LinearLayout layout = new LinearLayout(context);
                                        layout.setOrientation(LinearLayout.VERTICAL);
                                        View mView = getLayoutInflater().inflate(R.layout.dialog_content,null);
                                        // Actual fileName
                                        TextView currentFileName = (TextView) mView.findViewById(R.id.long_click_fileName);
                                        currentFileName.setText(currentFile.getFileName());

                                        // Actual FileSize
                                        TextView currentFileSize = (TextView)mView.findViewById(R.id.long_click_fileSize);
                                        String checker_size = new OnFileItemAction().getItemSize(currentFile.getFileSize());
                                        currentFileSize.setText(checker_size);

                                        // Actual FileDate
                                        TextView currentFileDate = (TextView)mView.findViewById(R.id.long_click_fileDate);
                                        TextView textFileDate = (TextView) mView.findViewById(R.id.text_fileDate);
                                        String checker_date = new OnFileItemAction().getFormatTime(currentFile.getFileDate());
                                        currentFileDate.setText(checker_date);
                                        if (checker_date.length()<8){
                                            String text= "Envoyé il y a";
                                            textFileDate.setText(text);

                                        }



                                        TextView download = (TextView)mView.findViewById(R.id.long_click_download);
                                        TextView delete = (TextView)mView.findViewById(R.id.long_click_delete);

                                        download.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                dialog.dismiss();
                                                Toast.makeText(context,"Téléchargement achevée",Toast.LENGTH_LONG).show();
                                            }
                                        });
                                        delete.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                dialog.dismiss();
                                                new UserAction().deleteFile(context,currentFile.getFileName(),
                                                        currentFile.getFileId(),getUserInfo()[1]);

                                            }
                                        });

                                        dialog.setContentView(mView);
                                        dialog.show();

                                        return true;
                                    }
                                });
                                //Fin du LongClick
                            }
                            //Fin du Else
                        }
                        //Fin du run du Handler
                    });
                    // Fin du Handler Total

                }
            }).start();
        }
        catch (Exception e){
            showToast("Les fichiers enrégistrés dans la base de données ne sont pas disponibles actuelllemnt");

        }
        this.upload_button.setOnClickListener(this);

    }
    @Override
    public void onClick(View v) {
        askPermissionAndBrowseFile();
    }


    private void askPermissionAndBrowseFile()  {
        // Asking Permission To access to Storage

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) { // Level 23

            // Check if we have Call permission
            int permission = ActivityCompat.checkSelfPermission(this.getContext(),
                    Manifest.permission.READ_EXTERNAL_STORAGE);

            if (permission != PackageManager.PERMISSION_GRANTED) {
                // If don't have permission so prompt the user.
                this.requestPermissions(
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        MY_REQUEST_CODE_PERMISSION
                );
                return;
            }
        }
        this.BrowseFile();
    }


    private void BrowseFile()  {
        Intent chooseFileIntent = new Intent(Intent.ACTION_GET_CONTENT);
        chooseFileIntent.setType("*/*");
        // Only return URIs that can be opened with ContentResolver
        chooseFileIntent.addCategory(Intent.CATEGORY_OPENABLE);

        chooseFileIntent = Intent.createChooser(chooseFileIntent, "Choose a file");
        startActivityForResult(chooseFileIntent, MY_RESULT_CODE_FILECHOOSER);
    }

    // When you have the request results
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        //
        switch (requestCode) {
            case MY_REQUEST_CODE_PERMISSION: {

                // Note: If request is cancelled, the result arrays are empty.
                // Permissions granted (CALL_PHONE).
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    Log.i(LOG_TAG, "Permission granted!");
                    showToast("Permission acceptée");
                    this.BrowseFile();
                }
                // Cancelled or denied.
                else {
                    Log.i(LOG_TAG, "Permission denied!");
                    showToast("Permission refusée");

                }
                break;
            }
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        String[] user_info = getUserInfo();
        uploadFileToServer = new UploadFileToServer();
        Log.d("User Info", "Voici l'utilisateur connecté************************** "+ user_info);

        if (requestCode == MY_RESULT_CODE_FILECHOOSER){
            if (resultCode == RESULT_OK ) {

                if(data != null)  {
                    Uri uri = data.getData();

                    String filePath = null;
                    try {
                        filePath = FilePathSender.getPath(this.getContext(),uri);
                        uploadFileToServer.MultipartRequest(this.getContext(),filePath,user_info);
                        Log.i("Get path of File", "This file path is " +filePath);
                        showToast("Début du téléchargement");

                    }
                    catch (Exception e) {
                        Log.e("Get path of File","This file upload fail. The error is : " + e);
                        showToast("Erreur de téléchargement du fichier ");

                    }

                }
            }
            else {
                showToast("Aucun fichier choisi");


            }

        }
    }


    public String [] getUserInfo() {
        MainActivity mainActivity=(MainActivity)getActivity();
        String user_Info[]=mainActivity.userInfo();
        return user_Info;
    }

    public void showToast(String Text){
        Toast.makeText(this.getContext(), Text , Toast.LENGTH_SHORT).show();
    }

    public void StartIntentToViewFile( String fileType, String fileUrl){
        Log.d(LOG_TAG,fileUrl);
        if (fileType.equalsIgnoreCase("image")){
            Uri uri=Uri.parse(fileUrl);
            Intent intent=new Intent(Intent.ACTION_VIEW);
            intent.setDataAndType(uri,"image/*");
            startActivity(intent);
        }


        else if (fileType.equalsIgnoreCase("video")){
            Uri uri=Uri.parse(fileUrl);
            Intent intent=new Intent(Intent.ACTION_VIEW);
            intent.setDataAndType(uri,"video/*");
            startActivity(intent);

        }

        else if (fileType.equalsIgnoreCase("audio")){
            Uri uri=Uri.parse(fileUrl);
            Intent intent=new Intent(Intent.ACTION_VIEW);
            intent.setDataAndType(uri,"audio/*");
            startActivity(intent);

        }
        else {
            Uri uri=Uri.parse(fileUrl);
            Intent intent=new Intent(Intent.ACTION_VIEW,uri);
            intent.setDataAndType(uri,"application/pdf");
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);

            try {
                startActivity(intent);
            }
            catch (ActivityNotFoundException e){
                showToast("Nous ne pouvons pas lire ce fichier");
            }
        }

    }


}