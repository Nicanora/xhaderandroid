package com.example.xhader.callback;
// This Interface is used to communicate between  MainActivity and Profile Fragment in which we are doing Logout
public interface LogoutListener {
    void logout();
}
