package com.example.xhader.callback;

// This Interface is used to communicate between Previous MainActivity and his different Fragments
public interface FragmentListener {
    void changeFragment(String page);
    void sendRegisterInformation(String pseudo, String email, String password, String confirm_pass);
    void sendLoginInformation(String pseudo, String password);
    void sendUpdatePasswordInformation(String pseudo, String password);

}
