package com.example.xhader.action;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;

import com.example.xhader.entities.User;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
// This class contain all user Action like Register, Login, Delete File, Change Password and Delete File.

public class UserAction {

    public String registerUser(Context context, String pseudo, String email, String password, String confirm_pass){
        String final_message = "nothing yet";
        String url_Register = "http://192.168.43.125/Xhader/Register.php";
        OkHttpClient client = new OkHttpClient();
        Log.d("Client", "run:client " + pseudo);

        RequestBody body = new FormBody.Builder()
                .add("pseudo",pseudo)
                .add("email",email)
                .add("password",password)
                .add("confimation_password",confirm_pass)
                .build();
        Log.d("Formbody", "run:Formbody ");

        Request request = new Request.Builder()
                .url(url_Register)
                .post(body)
                .build();
        Log.d("request", "Request was created"+request);


        try {
            Response response = client.newCall(request).execute();
            String result = response.body().string();
            Log.d("Response", "Response was created with sucess" + result);

            try {
                JSONObject user_informations = new JSONObject(result);
                String message = (String) user_informations.get("message");
                Log.d("JSONOBject", "User Informations" +message);

                if (message.equalsIgnoreCase("Insription success")){
                    showToast(context, "Compte compte créé avec succès  " + pseudo + "Connectez vous !");
                    final_message = "win";
                }
                //Error on Pseudo Field
                else if (message.equalsIgnoreCase("False pseudo")) {
                    showToast(context,"Le pseudo renseigné est invalide");
                }
                else if (message.equalsIgnoreCase("Exist pseudo")) {
                    showToast(context,"Un compte a été déja créé avec ce pseudo. Veuillez en trouver un autre.");
                }

                //Error on Email Field
                else if (message.equalsIgnoreCase("Exist Email")){
                    showToast(context,"Un compte existe déjà avec cet email");
                }

                else if (message.equalsIgnoreCase("False Email")) {
                    showToast(context,"Le email renseigné est invalide");
                }
                //Error on Password Field
                else if (message.equalsIgnoreCase("Different Password")){
                    showToast(context,"Les mots de passe doivent être identiques");
                }
                // Unknown Error or Server Error
                else if (message.equalsIgnoreCase("Inscription error")){
                    showToast(context," Une erreur s'est produite  lors de votre l'inscription. Veuillez réssayer plus tard.");
                }



                else{
                    showToast(context,"Veuillez remplir tous les champs ");
                }
            }
            catch (JSONException e){
                Log.d("JSonObject", "User Informations token failed");
                e.printStackTrace();
            }

        }

        catch (IOException e) {
            Log.d("Response", "Response was failed ");
            e.printStackTrace();
            showToast(context,"Impossible de se connecter à  Internet. Veuillez réessayer ultérieurement !");
        }

        return  final_message;
    }


    public User loginUser(Context context, String pseudo, String password) {
        User user = new User();
        String url_login = "http://192.168.43.125/Xhader/Login.php";
        OkHttpClient client = new OkHttpClient();
        Log.d("Client", "run:client " + pseudo);

        RequestBody body = new FormBody.Builder()
                .add("pseudo", pseudo)
                .add("password", password)
                .build();
        Log.d("Formbody", "run:Formbody ");

        Request request = new Request.Builder()
                .url(url_login)
                .post(body)
                .build();

        Log.d("Request", "Request was created with sucess " + request);

        try {
            Response response = client.newCall(request).execute();
            String result = response.body().string();
            Log.d("Response", "Response was created with sucess" + result);

            try {
                JSONObject user_informations = new JSONObject(result);
                final String message = ((String) user_informations.get("message"));
                Log.d("JSONOBject", "User Informations" + message);

                if (message.equalsIgnoreCase("Welcome")) {

                    showToast(context, "Bienvenue " + pseudo);

                    user.setId_user ( (String) user_informations.get("id"));
                    user.setPseudo((String) user_informations.get("pseudo"));
                    user.setEmail( (String) user_informations.getString("email"));
                    user.setPassword((String) user_informations.get("password"));
                }

                else if (message.equalsIgnoreCase("Incorrect Password")) {
                    showToast(context, "Mot de passe incorrect");

                }

                else if (message.equalsIgnoreCase("Incorrect Pseudo")) {
                    showToast(context, "Pseudo ou mot de passe incorrect");

                }

                else {
                    showToast(context, "Veuillez remplir tous les champs ");
                }
            }
            catch (JSONException e) {
                e.printStackTrace();
            }
        }
        catch (IOException e) {
            e.printStackTrace();
            showToast(context,"Impossible de se connecter à Internet. Veuillez réessayer ultérieurement");
        }

        return user;
    }

    public String changerUserPassword(Context context,String pseudo,String password){

        String final_message = "nothing yet";
        String url_Register = "http://192.168.43.125/Xhader/PasswordForget.php";
        OkHttpClient client = new OkHttpClient();
        Log.d("Client", "run:client " + pseudo);


        RequestBody body = new FormBody.Builder()
                .add("pseudo",pseudo)
                .add("password",password)
                .build();
        Log.d("Formbody", "run:Formbody ");

        Request request = new Request.Builder()
                .url(url_Register)
                .post(body)
                .build();
        Log.d("request", "Request was created"+request);


        try {
            Response response = client.newCall(request).execute();
            String result = response.body().string();
            Log.d("Response", "Response was created with sucess" + result);

            try {
                JSONObject user_informations = new JSONObject(result);
                String message = (String) user_informations.get("message");
                Log.d("JSONOBject", "User Informations" +message);

                // Success Message
                if (message.equalsIgnoreCase("Change success")){
                    showToast(context, "Mot de passe changé avec succès ! " + pseudo + " Connectez vous !");
                    final_message = "win";
                }
                // Pseudo Error
                else if (message.equalsIgnoreCase("Not existing pseudo")) {
                    showToast(context,"Aucun compte n'a été retrouvé avec ce pseudo");
                }

                else{
                    showToast(context,"Veuillez remplir tous les champs ");
                }
            }
            catch (JSONException e){
                Log.d("JSonObject", "User Informations token failed");
                e.printStackTrace();
            }

        }

        catch (IOException e) {
            Log.d("Response", "Response was failed ");
            e.printStackTrace();
            showToast(context,"Impossible de se connecter à  Internet. Veuillez réessayer ultérieurement !");
        }

        return  final_message;
    }

    public void deleteFile(final Context context, final String fileName, final String id, final String currentUserName){

        new Thread(new Runnable() {
            @Override
            public void run() {

                String url_Register = "http://192.168.43.125/Xhader/Deleted_file.php";
                OkHttpClient client = new OkHttpClient();
                Log.d("Client", "run:client for delete File With id " + id);


                RequestBody body = new FormBody.Builder()
                        .add("id",id)
                        .add("currentUserName",currentUserName)
                        .build();
                Log.d("Formbody", "run:Formbody ");

                Request request = new Request.Builder()
                        .url(url_Register)
                        .post(body)
                        .build();
                Log.d("request", "Request was created"+request);


                try {
                    Response response = client.newCall(request).execute();
                    String result = response.body().string();
                    Log.d("Response", "Response was created with sucess" + result);

                    try {
                        JSONObject user_informations = new JSONObject(result);
                        String message = (String) user_informations.get("message");
                        Log.d("JSONOBject", "User Informations" +message);

                        // Success Message
                        if (message.equalsIgnoreCase("Delete Success")){
                            showToast(context, "Fichier  " + fileName + " Supprimé");

                        }
                        // Pseudo Error
                        else if (message.equalsIgnoreCase("Unable Delete")) {
                            showToast(context,"Vous ne pouvez pas supprimer ce fichier car vous n'etes pas l'envoyeur.");
                        }

                        else{
                            showToast(context,"Erreur lors de la suppression");
                        }
                    }
                    catch (JSONException e){
                        Log.d("JSonObject", "User Informations token failed");
                        e.printStackTrace();
                    }

                }

                catch (IOException e) {
                    Log.d("Response", "Response was failed ");
                    e.printStackTrace();
                    showToast(context,"Impossible de se connecter à  Internet. Veuillez réessayer ultérieurement !");
                }
            }
        }).start();

    }

// ShowToast Method show Toast in a thread.

    public void showToast(final Context context, final String Text) {

        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(context, Text, Toast.LENGTH_LONG).show();
            }
        });
    }
}
