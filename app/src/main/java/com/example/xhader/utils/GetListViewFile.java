package com.example.xhader.utils;

import android.content.Context;
import android.util.Log;

import com.example.xhader.action.UserAction;
import com.example.xhader.entities.FileItem;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class GetListViewFile {
    public List<FileItem> GetFile(Context context) {
        final List<FileItem> fileItemList = new ArrayList<FileItem>();
        final String get_file_url = "http://192.168.43.125/Xhader/GetAllFile.php";

        OkHttpClient client = new OkHttpClient();
        Log.d("Client", "Client create");

        Log.d("Formbody", "run:Formbody ");

        Request request = new Request.Builder()
                .url(get_file_url)
                .build();
        Log.d("request", "Request was created" + request);

        try {
            Response response = client.newCall(request).execute();
            String result = response.body().string();
            Log.d("ResponseGettingFile","This is my File Getting response" +result);

            try
            {
                JSONArray file_list = new JSONArray(result);
                Log.d("File List", "GetFile:*********************************************** " +file_list);
                for (int i =0; i<file_list.length();i++){
                    JSONObject file_item= (JSONObject) file_list.get(i);
                    fileItemList.add(new FileItem(file_item.getString("id"),file_item.getString("name_file"),
                            file_item.getString("file_type"),
                            file_item.getString("file_size"),
                            file_item.getString("sendername"),
                            file_item.getString("file_url"),
                            file_item.getString("upload_date")));
                }

            }
            catch (JSONException e)
            {
                e.printStackTrace();
                Log.d("JSON", "JSON: Can not get JSONObject "+e);
            }
        }
        catch (IOException e)
        {
            e.printStackTrace();
            Log.d("Response", "GetFile: Your response was failed ");
            new UserAction().showToast(context,"Les fichiers enrégistrés dans la base de données ne sont pas disponibles actuelllemnt");
        }
        return  fileItemList;
    }


}
